<header>
    <section class="py-3 d-flex align-items-center justify-content-center gap-4">
        <a href="#home-hero">Home</a>
        <a href="#about">About</a>
        <a href="#packages">Packages</a>
        <div class="logo fw-bold" style="font-size: 24px;">
            DriverMaker
        </div>
        <a href="#trainers">Trainers</a>
        <a href="#vehicles">Vehicles</a>
        <a href="#contact-us">Contact Us</a>
    </section>
</header>