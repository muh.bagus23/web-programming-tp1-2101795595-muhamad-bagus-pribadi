@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
<style>
    .swiper {
        width: 100%;
        height: 100%;
      }

      .swiper-slide {
        font-size: 18px;
        background: #fff;
      }

      .swiper-slide img {
        display: block;
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
</style>
@endsection

@section('content')
<section id="home-hero">
    <div class="wrapper d-flex parallax" style="background-image: url({{ asset('assets/images/banner.jpeg') }});">
        <div class="layer-black"></div>
        <div class="content container">
            <div class="caption text-center mb-5">
                <h1>Learn to Drive <span class="fw-bold"></span></h1>
                <small>We will help you to increase your driving skills with guidance by our driving experts</small>
            </div>
            <div class="mouse_scroll mt-5">
                <div class="mouse">
                    <div class="wheel"></div>
                </div>
                <div>
                    <span class="m_scroll_arrows unu"></span>
                    <span class="m_scroll_arrows doi"></span>
                    <span class="m_scroll_arrows trei"></span>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about">
    <div class="container py-5">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6">
                <h1>DriverMaker</h1> 
                is a driving lesson company that already teach over +1000 professional drivers since 1999.
                We keep encourage and educate all the road users especially 4-wheels-driver to prioritize safety before anything.
                <br><br>
                We still grow and learn to reach even more prospective drivers around the world tbrough online nor offline.
                <hr>
                For all prospective driver who interest to try our service, this schedule below shows our operational driving lesson schedule
                <br><br>
                <ul class="list-group">
                    <li class="list-group-item">Weekday: 09:00 - 17:00</li>
                    <li class="list-group-item">Weekend: 09:00 - 15:00</li>
                </ul>
            </div>
            <div class="col-12 col-lg-6 p-5">
                <div class="img-cover" style="background-image: url({{ asset('assets/images/about-us.jpeg') }}); width: 100%; height: 600px; border-radius: 200px 2px 2px 60px;"></div>
            </div>
        </div>
    </div>
</section>

<section id="packages">
    <div class="bg-graphic"></div>
    <div class="container py-5">
        <div class="row">
            <div class="col-12 text-center mb-5 section-title">
                <h3>Our Packages</h3>
                <div>Choose the packages below that suits to your needs</div>
            </div>
            <!-- Free Tier -->
            <div class="col-12 col-lg-4">
                <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Copper</h5>
                        <h6 class="card-price text-center">Rp610.000</h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>6 Driving Lessons (1 hour each)</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Training Car (Matic/Manual)</li>
                        </ul>
                        <div class="d-grid pt-3">
                            <a href="#" class="btn btn-warning text-uppercase">Apply</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Silver</h5>
                        <h6 class="card-price text-center">Rp820.000</h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>10 Driving Lessons (1 hour each)</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Training Car (Matic/Manual/Mix)</li>
                        </ul>
                        <div class="d-grid pt-3">
                            <a href="#" class="btn btn-warning text-uppercase">Apply</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Gold</h5>
                        <h6 class="card-price text-center">Rp1.500.000</h6>
                        <hr>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>18 Driving Lessons (1 hour each)</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Training Car (Matic/Manual/Mix)</li>
                        </ul>
                        <div class="d-grid pt-3">
                            <a href="#" class="btn btn-warning text-uppercase">Apply</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="trainers">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 text-center mb-5 section-title">
                <h3>Our Trainers</h3>
                <div>All DriverMaker's trainers has more than 3 years experience teaching drivers</div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item">
                    <div class="img-cover" style="width: 100%; height: 400px; background-image: url({{ asset('assets/images/driver-1.jpeg') }});"></div>
                    <div class="desc p-2">
                        <div style="fw-bold">John</div>
                        <small>Certified Driver Trainer</small>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item">
                    <div class="img-cover" style="width: 100%; height: 400px; background-image: url({{ asset('assets/images/driver-2.jpeg') }});"></div>
                    <div class="desc p-2">
                        <div style="fw-bold">Raouf</div>
                        <small>Certified Driver Trainer</small>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item">
                    <div class="img-cover" style="width: 100%; height: 400px; background-image: url({{ asset('assets/images/driver-3.jpeg') }});"></div>
                    <div class="desc p-2">
                        <div style="fw-bold">Eddie</div>
                        <small>Certified Driver Trainer</small>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="item">
                    <div class="img-cover" style="width: 100%; height: 400px; background-image: url({{ asset('assets/images/driver-4.jpeg') }});"></div>
                    <div class="desc p-2">
                        <div style="fw-bold">Matt</div>
                        <small>Certified Driver Trainer</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="vehicles">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-5 section-title">
                <h3>Vehicles</h3>
                <div>We provide car for training, check vehicle below that fit with your needs</div>
            </div>
            <div class="col-12">
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide row d-flex align-items-center">
                            <div class="col-12 col-lg-8">
                                <div class="img-contain" style="width: 100%; height: 400px; background-image: url({{ asset('assets/images/car-1.png') }});"></div>
                            </div>
                            <div class="col-12 col-lg-4 text-left">
                                <h4>Hilux</h4>
                                <ul style="list-style: none; padding: 0;">
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Type</div>
                                        <div class="col-6 col-lg-7">: Pick Up Truck</div>
                                    </li>
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Transmission</div>
                                        <div class="col-6 col-lg-7">: Manual</div>
                                    </li>
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Year</div>
                                        <div class="col-6 col-lg-7">: 2018</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide row d-flex align-items-center">
                            <div class="col-12 col-lg-8">
                                <div class="img-contain" style="width: 100%; height: 400px; background-image: url({{ asset('assets/images/car-2.png') }});"></div>
                            </div>
                            <div class="col-12 col-lg-4 text-left">
                                <h4>Hilux</h4>
                                <ul style="list-style: none; padding: 0;">
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Type</div>
                                        <div class="col-6 col-lg-7">: Pick Up Truck</div>
                                    </li>
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Transmission</div>
                                        <div class="col-6 col-lg-7">: Matic</div>
                                    </li>
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Year</div>
                                        <div class="col-6 col-lg-7">: 2018</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide row d-flex align-items-center">
                            <div class="col-12 col-lg-8">
                                <div class="img-contain" style="width: 100%; height: 400px; background-image: url({{ asset('assets/images/car-3.png') }});"></div>
                            </div>
                            <div class="col-12 col-lg-4 text-left">
                                <h4>Honda</h4>
                                <ul style="list-style: none; padding: 0;">
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Type</div>
                                        <div class="col-6 col-lg-7">: Sedan</div>
                                    </li>
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Transmission</div>
                                        <div class="col-6 col-lg-7">: Matic</div>
                                    </li>
                                    <li class="row">
                                        <div class="col-6 col-lg-4">Year</div>
                                        <div class="col-6 col-lg-7">: 2018</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contact-us">
    <div class="bg-graphic"></div>
    <div class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center mb-5 section-title">
                    <h3>Contact Us</h3>
                    <div>If you're interested or have any question, please feel free to reach us!</div>
                </div>
                <div class="col-12 col-lg-4">
                    <h3>Offices & Locations</h3>
                    <hr>
                    <div class="mb-3">
                        <h5>Jakarta</h5>
                        Jl. Menteng Dalem, Kebayoran Lama, No. 45
                    </div>
                    <div class="mb-3">
                        <h5>Bogor</h5>
                        Jl. Pajajaran, Bangbarung Raya, No. 7
                    </div>
                    <div class="mb-3">
                        <h5>Bandung</h5>
                        Komp. Pegunungan Baru, Ciwidey, No. 12
                    </div>
                    <div class="mb-3">
                        <h5>Bali</h5>
                        Komp. Wisata Canggu, No. 9
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <h3>Contact</h3>
                    <hr>
                    Phone <a href="">(62) 123 45678</a>
                    <br>
                    Whatsapp <a href="">(62) 877 12345678</a>
                    <br>
                    Instagram <a href="">@drivermaker</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="footer">
    <div class="container text-center">
        Copyright &copy; DriverMaker 2022. All Rights Reserved.
    </div>
</section>
@endsection

@section('script')
<!-- Swiper JS -->
<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper(".swiper", {});
    // Setup Typewriter Home Hero
    var app = document.querySelector('#home-hero .caption span');

    var typewriter = new Typewriter(app, {
        loop: false
    });

    typewriter.typeString('Fast?')
        .pauseFor(2500)
        .deleteAll()
        .typeString('Safe?')
        .pauseFor(2500)
        .deleteAll()
        .typeString('Properly?')
        .pauseFor(3500)
        .deleteAll()
        .typeString('with DriverMaker.')
        .pauseFor(2500)
        .start();
</script>
@endsection